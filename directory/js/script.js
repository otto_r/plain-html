// CONTENT FORMATTING/INSERTION

var curSection = 0;
class ContentSection {
  constructor(bg, title, desc, inner) {
    if(desc === undefined) {
      this.html = bg;
      this.bg = title;
      this.custom = true;
    }
    else {
      this.bg = bg;
      this.title = title;
      this.desc = desc;
      this.inner = inner;
      this.custom = false;
    }
  }

  getCardHtml() {
    if(this.custom) {
      return this.html;
    }
    else {
      return `
        <div class="content-card">
          <h2>` + this.title + `</h2>
          <p>` + this.desc + `</p>
          <svg class="content-card-icon" onclick="contentCardToggle()" viewBox="0 0 34.83 18.83" xmlns="http://www.w3.org/2000/svg">
            <polygon points="25.41 0 22.59 2.83 27.17 7.41 0 7.41 0 11.41 27.17 11.41 22.59 16 25.41 18.83 34.83 9.41"/>
          </svg>
        </div>
        <div class="content-card content-card-bg content-card-bg-in">
          <h2>` + this.title + `</h2>
          <p>` + this.desc + `</p>
          <svg class="content-card-icon" viewBox="0 0 34.83 18.83" xmlns="http://www.w3.org/2000/svg">
            <polygon points="25.41 0 22.59 2.83 27.17 7.41 0 7.41 0 11.41 27.17 11.41 22.59 16 25.41 18.83 34.83 9.41"/>
          </svg>
          <svg class="content-card-icon content-card-icon-close" onclick="contentCardToggle()" viewBox="0 0 34.83 18.83" xmlns="http://www.w3.org/2000/svg">
            <polygon points="25.41 0 22.59 2.83 27.17 7.41 0 7.41 0 11.41 27.17 11.41 22.59 16 25.41 18.83 34.83 9.41"/>
          </svg>
        </div>
      `;
    }
  }

  getBg() {
    if(this.custom) {
      return this.bg;
    } else {
      return '<section class="content-section" style="background-image: url(\''+ this.bg + '\')"></section>';
    }
  }

  getInnerHtml() {
    if(!this.custom) {
      return this.inner;
    }
  }
}

function getTeamCards() {
  var teamCards = '';
  for(let i = 0; i < team.length; i++) {
    teamCards = teamCards.concat(`
      <div class="team-card">
        <div class="team-card-dp" style="background-image: url('images/team/` + team[i][2] + `')"></div>
        <div class="team-card-info">
          <span class="team-card-name">` + team[i][0] + `</span><br>
          <span class="team-card-designation">` + team[i][1] + `</span>
        </div>
      </div>
    `);
  }
  return teamCards;
}

const team = [
  ['Otto Rothmund', 'CEO & Founder', 'otto.jpg'],
  ['Krystoff Lastname', 'Example Developer', 'krystoff.jpg']
];
const sections = [
  new ContentSection(`
    <div id="landing-text">
      <h1>Staking Forums</h1>
      <p>Building decentralized infrastructure with a focus on privacy, freedom and efficiency.</p>
    </div>
    <div id="scroll-icon">
      <div id="scroll-wheel-icon"></div>
    </div>
  `, `
    <section class="content-section">
    <svg xmlns="http://www.w3.org/2000/svg"
    width="300" height="100" viewBox="0 0 300 100">
    <text x="0" y="40%" style="font: 18px sans-serif; inline-size: 250pt;">• • •</text>
    <text x="0" y="60%" style="font: 18px sans-serif; inline-size: 250pt">• • ⎯ •</text>
    </svg>
    canvas id="landing-bg"></canvas>
    </section>
  `),

  new ContentSection(' ./images/bg/bg-1.jpg', 'Philosophy', `We believe in the idea of "Don't trust - Verify!", that anonymity is a right, and that right can only be given up by the user. themselves, and other fundamental principles of early Cypherpunk.`),
  new ContentSection(' ./images/bg/bg-2.jpg',`Partners`, 'Our goal is to teach by example, as well as develop the infrastructure to prevent quick or low-quality solutions from causing layer-1 issues in the future.  Our partners'),
  new ContentSection(`
    <div id="team-container">
      ` + getTeamCards() + `
    </div>
  `, `
    <section class="content-section">
    </section>
  `)
];

// SCROLL MECHANICS + SECTION NAVIGATION

document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function getTouches(evt) {
  return evt.touches ||             // browser API
         evt.originalEvent.touches; // jQuery
}

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
};

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if (Math.abs(xDiff) < Math.abs(yDiff)) {
      if ( yDiff > 0 ) {
          sectionChange(1);
      }
      else {
          sectionChange(-1);
      }
    }

    xDown = null;
    yDown = null;
};

var canScroll = true;

$(window).on('mousewheel DOMMouseScroll', function(e) {
  console.log(e.originalEvent.deltaY);
  if(canScroll && Math.abs(e.originalEvent.deltaY) > 50) {
    canScroll = false;
    sectionChange(e.originalEvent.deltaY > 0 ? 1 : -1);
  }
});

function sectionChange(dir) {
  let lastSection = curSection;
  curSection += dir;
  curSection = Math.min(curSection, sections.length - 1);
  curSection = Math.max(curSection, 0);
  console.log('Last Section: ' + lastSection + ' curSection: ' + curSection);

  if(curSection != lastSection) {
    if(lastSection == 0 || curSection == 0) {
      $('header svg').removeClass('header-logo-small header-logo-large');
      $('header svg').addClass('header-logo-' + (lastSection == 0 ? 'out' : 'in') + '-anim');
      setTimeout(function() {
        if(lastSection == 0) {
          $('header svg').addClass('header-logo-small');
        }
        else {
          $('header svg').addClass('header-logo-large');
        }
        $('header svg').removeClass('header-logo-in-anim header-logo-out-anim');
      },600);
    }

    $('.content-overlay').addClass('content-overlay-' + (dir > 0 ? 'up' : 'down') + '-out');
    $('.content-overlay').after(`
      <section class="content-overlay content-overlay-` + (dir > 0 ? 'down' : 'up') + `-in">
        ` + sections[curSection].getCardHtml() + `
      </section>
    `);

    if(curSection > lastSection) {
      $('.content-section').css('animation-direction', 'reverse').addClass('content-section-up');
    } else {
      $('.content-section').css('animation-direction', 'reverse').addClass('content-section-down');
    }
    $('#content-bg').append(sections[curSection].getBg());
    console.log(sections[curSection].getBg());

    setTimeout(function() {
      $('.content-overlay-up-out, .content-overlay-down-out').remove();
      $('.content-overlay').removeClass('content-overlay-up-in').removeClass('content-overlay-down-in');
      $('.content-card-bg-in').removeClass('content-card-bg-in');
      canScroll = true;
      $('.content-section-up, .content-section-down').remove();
    },500);

    contentCardState = false;
  }
  else {
    canScroll = true;
  }
}

// CONTENT CARD ANIMATIONS

var contentCardState = false;

function contentCardToggle() {
  $('.content-card-expanded').animate({opacity: 0}, 200);
  var $contentCardIcon = $('.content-card-icon');
  var $contentCardBg = $('.content-card-bg');
  if(contentCardState) {
    $($contentCardIcon).css('animation-direction', 'reverse').addClass('content-card-icon-anim');
    $($contentCardBg).css('animation-direction', 'reverse').removeClass('content-card-bg-normal content-card-bg-expanded').addClass('content-card-bg-anim');
    $('.content-card').not('.content-card-bg').removeClass('content-card-fade-in content-card-fade-out').css('animation-direction', 'reverse').addClass('content-card-fade-anim');
    setTimeout(function() {
      $('.content-card-fade-anim').addClass('content-card-fade-in').removeClass('content-card-fade-anim');
      $('.content-card-expanded').remove();
      $($contentCardIcon).css('margin-right', '0');
      $($contentCardBg).addClass('content-card-bg-normal');
      $($contentCardIcon).removeClass('content-card-icon-anim');
      $($contentCardBg).removeClass('content-card-bg-anim');
      contentCardState = false;
    },500);
  }
  else {
    $('.content-overlay').append('<div class="content-card-expanded" style="opacity: 0">' + sections[curSection].getInnerHtml() + '</div>');
    $('.content-card-expanded').animate({opacity: 1}, 200);
    $($contentCardIcon).css('animation-direction', 'normal').addClass('content-card-icon-anim');
    $($contentCardBg).css('animation-direction', 'normal').removeClass('content-card-bg-normal content-card-bg-expanded').addClass('content-card-bg-anim');
    $('.content-card').not('.content-card-bg').removeClass('content-card-fade-in content-card-fade-out').css('animation-direction', 'normal').addClass('content-card-fade-anim');
    setTimeout(function() {
      $('.content-card-fade-anim').addClass('content-card-fade-out').removeClass('content-card-fade-anim');
      $($contentCardIcon).css('margin-right', '-6rem');
      $($contentCardBg).addClass('content-card-bg-expanded');
      $($contentCardIcon).removeClass('content-card-icon-anim');
      $($contentCardBg).removeClass('content-card-bg-anim');
      contentCardState = true;
    },500);
  }
}

// HEADER MENU

function handleHeaderMenu(targetSection) {
  let diff = targetSection - curSection;
  sectionChange(diff);
  console.log(diff);
}
